package com.example.sdima.recipease2.pojos;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;

import com.example.sdima.recipease2.enums.Category;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "recipe_categories",
        foreignKeys = {@ForeignKey(entity = Recipe.class, parentColumns = "id", childColumns = "recipe_id", onDelete = CASCADE)},
        primaryKeys = {"recipe_id", "category"})
public class RecipeCategories {

    @ColumnInfo(name = "recipe_id")
    private long recipeId;

    @Ignore
    private Category category;

    @ColumnInfo(name = "category")
    private int categoryOrdinal;


    public RecipeCategories() {

    }

    public RecipeCategories(long recipeId, int categoryOrdinal) {
        this.recipeId = recipeId;
        setCategoryOrdinal(categoryOrdinal);
    }

    public RecipeCategories(long recipeId, Category category) {
        this.recipeId = recipeId;
        setCategory(category);
    }

    public long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(long recipeId) {
        this.recipeId = recipeId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
        setCategoryOrdinal(category.ordinal());
    }

    public int getCategoryOrdinal() {
        return categoryOrdinal;
    }

    public void setCategoryOrdinal(int categoryOrdinal) {
        this.categoryOrdinal = categoryOrdinal;
        this.category = Category.getCategory(categoryOrdinal);
    }
}
