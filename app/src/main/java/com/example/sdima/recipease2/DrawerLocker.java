package com.example.sdima.recipease2;

public interface DrawerLocker {
    public void setDrawerEnabled(boolean enabled);
}
