package com.example.sdima.recipease2.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.sdima.recipease2.pojos.Ingredient;

import java.util.List;

@Dao
public interface IngredientDao {

    //Inserts
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insertIngredient(Ingredient ingredient);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    List<Long> insertIngredients(Ingredient[] ingredients);

    //Deletes
    @Delete
    void deleteIngredient(Ingredient ingredient);

    @Query("DELETE FROM ingredient WHERE id = :id")
    void deleteIngredientById(long id);

    @Query("DELETE FROM ingredient")
    void deleteAllIngredients();

    //Updates
    @Update
    int updateIngredient(Ingredient ingredient);

    //Queries
    @Query("SELECT * FROM ingredient ORDER BY name ASC")
    LiveData<List<Ingredient>> getAlphabetizedIngredients();

    @Query("SELECT * FROM ingredient ORDER BY id ASC")
    LiveData<List<Ingredient>> getAllIngredients();

    @Query("SELECT * FROM ingredient where id = :id")
    LiveData<Ingredient> getIngredientByID(long id);

    @Query("SELECT * FROM ingredient where name = :name")
    LiveData<Ingredient> getIngredientByName(String name);

    @Query("SELECT * FROM ingredient where name LIKE '%' || :search || '%'")
    LiveData<List<Ingredient>> getIngredientsWithNamesLike(String search);

    @Query("SELECT * FROM ingredient where name LIKE :search || '%'")
    LiveData<List<Ingredient>> getIngredientsStartingWith(String search);
}
