package com.example.sdima.recipease2.ui.viewrecipes;

import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sdima.recipease2.R;
import com.example.sdima.recipease2.pojos.Recipe;
import com.example.sdima.recipease2.ui.viewrecipes.recipeadapter.RecipeAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class ViewRecipesFragment extends Fragment {

    private ViewRecipesViewModel mViewModel;
    private RecyclerView recipeRecyclerView;

    public static ViewRecipesFragment newInstance() {
        return new ViewRecipesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(ViewRecipesViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_recipes_fragment, container, false);
        recipeRecyclerView = v.findViewById(R.id.recipes_recycler_view);
        RecipeAdapter myAdapter = new RecipeAdapter(getActivity(), mViewModel.getRecipes(), mViewModel);
        recipeRecyclerView.setAdapter(myAdapter);
        recipeRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mViewModel.getRecipesCollectionLive().observe(getViewLifecycleOwner(), recipes -> {
            if(recipes != null) {
                setRecipeRecyclerViewAdapter(myAdapter, recipes);
            }
        });

        FloatingActionButton fab = v.findViewById(R.id.add_recipe_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(v);
                navController.navigate(R.id.action_nav_recipes_fragment_to_nav_new_recipe_fragment);
            }
        });
        return v;
    }

    public void setRecipeRecyclerViewAdapter(RecipeAdapter myAdapter, List<Recipe> recipes){
        myAdapter.setRecipes(recipes);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        mViewModel.updateRecipesList();
        super.onStart();
    }
}