package com.example.sdima.recipease2.enums;

import java.util.ArrayList;
import java.util.List;

/**
 * The following enum is an enum representing the category an object fits into.
 */
public enum Category {
    Chicken,
    Pork,
    Fish,
    Beef,
    Soup,
    Appetizer,
    Main,
    Starter,
    Salad,
    Dessert,
    Drink,
    Side,
    Dressing,
    Other
    ;

    public static Category getCategory(int category) {
        Category result;
        switch(category) {
            case 0:
                result = Chicken;
                break;
            case 1:
                result = Pork;
                break;
            case 2:
                result = Fish;
                break;
            case 3:
                result = Beef;
                break;
            case 4:
                result = Soup;
                break;
            case 5:
                result = Appetizer;
                break;
            case 6:
                result = Main;
                break;
            case 7:
                result = Starter;
                break;
            case 8:
                result = Salad;
                break;
            case 9:
                result = Dessert;
                break;
            case 10:
                result = Drink;
                break;
            case 11:
                result = Side;
                break;
            case 12:
                result = Dressing;
                break;
            default:
                result = Other;
        }
        return result;
    }

    public static List<String> getAllCategoryStrings(){
        List<String> allCategories = new ArrayList<String>();
        for(Category c : Category.values()){
            allCategories.add(c.name());
        }
        return allCategories;
    }
}
