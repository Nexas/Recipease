package com.example.sdima.recipease2.complexobjects;

import com.example.sdima.recipease2.pojos.Ingredient;
import com.example.sdima.recipease2.pojos.IngredientList;
import com.example.sdima.recipease2.pojos.Recipe;
import com.example.sdima.recipease2.pojos.RecipeDirections;

import java.util.ArrayList;

/**
 * This represents a Full Recipe that will be used to render new pages in the application.
 */
public class FullRecipe {
	/**
	 * The recipe that is being used
	 */
	private Recipe recipe;

	/**
	 * The list of IngredientLists associated with this recipe.
	 */
	private ArrayList<IngredientList> ingredientLists;

	/**
	 * The RecipeDirections associated with this recipe.
	 */
	private ArrayList<RecipeDirections> recipeDirections;

	/**
	 * The ingredients associated with this recipe.
	 */
	private ArrayList<Ingredient> ingredients;

	public FullRecipe(){

	}
	public FullRecipe(Recipe recipe, ArrayList <IngredientList> ingredientLists, ArrayList <RecipeDirections> recipeDirections, ArrayList<Ingredient> ingredients){
		this.recipe = recipe;
		this.ingredientLists = ingredientLists;
		this.recipeDirections = recipeDirections;
		this.ingredients = ingredients;
	}

	public Recipe getRecipe() {
		return recipe;
	}
	public void setRecipe(Recipe recipe) {
		this.recipe = recipe;
	}

	public ArrayList<RecipeDirections> getRecipeDirections() {
		return recipeDirections;
	}
	public void setRecipeDirections(ArrayList<RecipeDirections> recipeDirections) {
		this.recipeDirections = recipeDirections;
	}

	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}
	public void setIngredients(ArrayList<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public ArrayList<IngredientList> getIngredientLists() {
		return ingredientLists;
	}
	public void setIngredientLists(ArrayList<IngredientList> ingredientLists) {
		this.ingredientLists = ingredientLists;
	}
}
