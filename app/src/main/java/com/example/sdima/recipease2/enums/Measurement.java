package com.example.sdima.recipease2.enums;

import java.util.ArrayList;
import java.util.List;

public enum Measurement {
    cup,
    tsp,
    tbsp,
    ml,
    L,
    g,
    kg,
    oz,
    bunch,
    bundle,
    handful,
    dash,
    toTaste,
    lb,
    NA
    ;

    public static Measurement getMeasurement(int measurementID){
        Measurement result;
        switch (measurementID){
            case 0:
                result = cup;
                break;
            case 1:
                result = tsp;
                break;
            case 2:
                result = tbsp;
                break;
            case 3:
                result = ml;
                break;
            case 4:
                result = L;
                break;
            case 5:
                result = g;
                break;
            case 6:
                result = kg;
                break;
            case 7:
                result = oz;
                break;
            case 8:
                result = bunch;
                break;
            case 9:
                result = bundle;
                break;
            case 10:
                result = handful;
                break;
            case 11:
                result = dash;
                break;
            case 12:
                result = toTaste;
                break;
            case 13:
                result = lb;
                break;
            default:
                result = NA;
        }
        return result;
    }

    public static List<Measurement> getAllMeasurements(){
        List<Measurement> allMeasurements = new ArrayList<Measurement>();
        for(Measurement m : Measurement.values()){
            allMeasurements.add(m);
        }
        return allMeasurements;
    }

    public static List<String> getAllMeasurementsStrings(){
        List<String> allMeasurements = new ArrayList<String>();
        for(Measurement m : Measurement.values()){
            allMeasurements.add(m.name());
        }
        allMeasurements.remove("NA");
        return allMeasurements;
    }

    public static double convertToGrams(int measurementID, double quantity){
        if(measurementID == 6){
            return 1000*quantity;
        }
        else if(measurementID == 13){
            return 454*quantity;
        }
        else{
            return -1;
        }
    }

    public static double convertToKg(int measurementID, double quantity){
        if(measurementID == 5)
        {
            return quantity/1000;
        }
        else if (measurementID == 13)
        {
            return 0.454*quantity;
        }
        else{
            return -1;
        }
    }

    //TODO implement this method
    public static double convertToLb(int measurementID, double quantity){ return -1; }

    public static double convertToMl(int measurementID, double quantity){
        if (measurementID == 0){
            return 250*quantity;
        }
        else if(measurementID ==1){
            return 5*quantity;
        }
        else if(measurementID==2){
            return 15*quantity;
        }
        else if(measurementID == 4){
            return 1000*quantity;
        }
        else{
            return -1;
        }
    }

    public static double convertToL(int measurementID, double quantity){
        if(measurementID == 0){
            return 0.25*quantity;
        }
        else if(measurementID == 1){
            return 0.005*quantity;
        }
        else if(measurementID == 2){
            return 0.015*quantity;
        }
        else if(measurementID == 3){
            return 0.001*quantity;
        }
        else{
            return -1;
        }
    }

    //TODO implement this method
    public static double convertToTbsp(int measurementID, double quantity){
        return -1;
    }

    //TODO implement this method
    public static double convertToTsp(int measurementID, double quantity){
        return -1;
    }
}
