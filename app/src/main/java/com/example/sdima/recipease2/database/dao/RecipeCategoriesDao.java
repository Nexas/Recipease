package com.example.sdima.recipease2.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.sdima.recipease2.pojos.RecipeCategories;

import java.util.List;

@Dao
public interface RecipeCategoriesDao {

    //Inserts
    @Insert
    void insertRecipeCategories(RecipeCategories recipeCategories);

    @Insert
    void insertRecipeCategories(RecipeCategories[] recipeCategories);

    //Deletes
    @Delete
    void deleteRecipeCategories(RecipeCategories recipeCategories);

    @Query("DELETE FROM recipe_categories")
    void deleteAllRecipeCategories();

    @Query("DELETE FROM recipe_categories WHERE recipe_id = :id")
    void deleteAllRecipeCategoriesByRecipeId(long id);

    //Queries
    @Query("SELECT * FROM recipe_categories WHERE recipe_id = :id")
    LiveData<List<RecipeCategories>> getRecipeCategoriesByRecipeId(long id);

}
