package com.example.sdima.recipease2.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.sdima.recipease2.database.dao.IngredientDao;
import com.example.sdima.recipease2.database.dao.IngredientListDao;
import com.example.sdima.recipease2.database.dao.RecipeCategoriesDao;
import com.example.sdima.recipease2.database.dao.RecipeDao;
import com.example.sdima.recipease2.database.dao.RecipeDirectionsDao;
import com.example.sdima.recipease2.pojos.Ingredient;
import com.example.sdima.recipease2.pojos.IngredientList;
import com.example.sdima.recipease2.pojos.Recipe;
import com.example.sdima.recipease2.pojos.RecipeCategories;
import com.example.sdima.recipease2.pojos.RecipeDirections;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Ingredient.class, Recipe.class, IngredientList.class, RecipeDirections.class, RecipeCategories.class}, version = 1, exportSchema = false)
public abstract class RecipeaseDatabase extends RoomDatabase {

    public abstract IngredientDao ingredientDao();
    public abstract IngredientListDao ingredientListDao();
    public abstract RecipeDao recipeDao();
    public abstract RecipeDirectionsDao recipeDirectionsDao();
    public abstract RecipeCategoriesDao recipeCategoriesDao();


    private static volatile RecipeaseDatabase INSTANCE;

    //Sets a finite number of threads to be able to be used by the Db
    private static final int NUMBER_OF_THREADS = 8;
    static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };

    public static RecipeaseDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RecipeaseDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RecipeaseDatabase.class, "recipease_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }


}
