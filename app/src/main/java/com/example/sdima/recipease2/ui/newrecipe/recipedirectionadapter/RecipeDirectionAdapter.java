package com.example.sdima.recipease2.ui.newrecipe.recipedirectionadapter;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.sdima.recipease2.R;
import com.example.sdima.recipease2.enums.Measurement;
import com.example.sdima.recipease2.pojos.Ingredient;
import com.example.sdima.recipease2.pojos.IngredientList;
import com.example.sdima.recipease2.pojos.RecipeDirections;
import com.example.sdima.recipease2.ui.newrecipe.NewRecipeFragmentViewModel;

import java.util.List;

public class RecipeDirectionAdapter extends ArrayAdapter<RecipeDirections> {

    private Context context;
    private List<RecipeDirections> recipeDirections;
    private NewRecipeFragmentViewModel mViewModel;


    public RecipeDirectionAdapter(@NonNull Context context, @NonNull List<RecipeDirections> recipeDirections, @NonNull NewRecipeFragmentViewModel mViewModel) {
        super(context, R.layout.recipe_directions_row, recipeDirections);
        this.recipeDirections = recipeDirections;
        this.context = context;
        this.mViewModel = mViewModel;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        convertView = inflater.inflate(R.layout.recipe_directions_row, parent, false);

        EditText recipeDirection = convertView.findViewById(R.id.recipe_direction_edit_text);
        renderAddButton(convertView, position);
        setInitialValues(convertView, position, recipeDirection);
        renderRemoveButton(convertView, position, recipeDirection);

        //TODO should this be on lost focus instead?
        recipeDirection.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i("RecipeDirection Adapter" , " IngredientName: in beforeTextChanged");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i("RecipeDirection Adapter" , " IngredientName: in beforeTextChanged");
            }

            @Override
            public void afterTextChanged(Editable s) {
                mViewModel.getRecipeDirections().get(position).setStepDirections(s.toString());
            }
        });

        return convertView;
    }

    //Used to update the data in the adapter
    public void setData(List<RecipeDirections> recipeDirections) {
        this.recipeDirections = recipeDirections;
        notifyDataSetChanged();
    }

    //Helper methods
    private void renderRemoveButton(View convertView, int position, EditText recipeDirection) {
        ImageButton removeRecipeDirectionButton = convertView.findViewById(R.id.remove_recipe_direction);
        removeRecipeDirectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(position ==0 && recipeDirections.size() ==1){
                    recipeDirection.setText("");
                }
                else{
                    mViewModel.removeRecipeDirections(position);
                }
            }
        });
    }

    private void renderAddButton(View convertView, int position) {
        ConstraintLayout addRecipeDirectionButtonLayout = convertView.findViewById(R.id.add_recipe_direction_constraint_layout);
        if(position!=recipeDirections.size()-1){
            addRecipeDirectionButtonLayout.setVisibility(View.GONE);
        }
        else{
            if(((Activity)context).getCurrentFocus()!=null) {
                ((Activity)context).getCurrentFocus().clearFocus();
            }
            ImageButton addRecipeDirectionButton = convertView.findViewById(R.id.add_recipe_direction_button);
            addRecipeDirectionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewModel.addRecipeDirections();
                }
            });
        }
    }

    private void setInitialValues(View convertView, int position, EditText recipeDirection){
        if(mViewModel.getRecipeDirections().get(position).getStepDirections()!=null){
            recipeDirection.setText(mViewModel.getRecipeDirections().get(position).getStepDirections());
        }
        TextView recipeDirectionNumber = convertView.findViewById(R.id.recipe_direction_step_number_label);
        recipeDirectionNumber.setText((position+1)+".");
    }
}
